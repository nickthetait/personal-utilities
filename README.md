# Overview
Series of security or time saving utilities that I built for personal use. Written in Python with no external dependencies. Tested on Ubuntu (16.04 LTS) with Python (3.6.3). No guarantees are made that these tools will be useful or work on your platform at all.

## VPN
Utilities for starting up a VPN service. One to select a config that you use often and another to randomly pick a location to masquerade as.

## Security Chihuahua
![Logo](security-chihuahua/logo.png)

Helps protect your data if your device is stolen/seized while in an unlocked state. Defeats hardware or software based "mouse jigglers" by triggering system lockout or shutdown at a specified time interval. Makes no changes to the operating system itself. Since it is so strict and occurs without warning, this tool is EXTREMELY disruptive even to authorized users!

Most effective when:

- automatically (and silently) invoked on login
- used in conjunction with full disk encryption

## Document Launcher
Really simple, just opens up a set of frequently used files.
