import os
import random
import subprocess
import argparse
import getpass

def main():
    parser = argparse.ArgumentParser("python3 location-randomizer.py")
    parser.add_argument("directory",
        help="Directory to search for VPN configurations", type=str)
    search_dir = parser.parse_args().directory
    extension = ".ovpn"

    good_files = []
    for root, directory, filenames in os.walk(search_dir):
        for item in filenames:
            if item.endswith(extension):
                good_files.append(item)

    count = len(good_files)
    if count > 0:
        print("Found {} candidates in ({})".format(count, search_dir))
        acceptable_selection = False
        while(acceptable_selection != True):
            selection = random.choice(good_files)
            prompt = "{} randomly selected. Enter to".format(selection) + \
                     " accept or type anything to re-roll "

			#Any data a user enters here is irrelevant,
			#but don't display it just incase they enter their password 🙈
            if not getpass.getpass(prompt):
                acceptable_selection = True

        full_path_to_selection = os.path.join(search_dir, selection)
        final_command = ["sudo", "openvpn", "--auth-nocache", "--config", full_path_to_selection]
        subprocess.call(final_command)
    else:
        print("Failed to find any candidates in ({})".format(search_dir))

main()
