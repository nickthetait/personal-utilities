import subprocess

def read_file_list(path):
    files = []
    with open(path, 'r') as file:
        for line in file:
            files.append(line.rstrip())
    file.closed
    return(files)

def build_command(files):
    command = ["gedit"]
    for item in files:
        command.append(item)
    return command

def main():
    path = "test/file-list.txt"
    list_of_files = read_file_list(path)
    command = build_command(list_of_files)
    subprocess.Popen(command)

main()
