import subprocess
import argparse

def get_args():
    parser = argparse.ArgumentParser("python3 utility.py")
    parser.add_argument("extension",
        help="File type extension", type=str)
    parser.add_argument("text",
        help="Text to find", type=str)

    extension = parser.parse_args().extension
    text = parser.parse_args().text
    return(extension, text)

def build_command():
    ext, search = get_args()
    #find -iname '*.extension' -exec grep --color=always -iH "text" {} \;
    command = ["find", "-iname", ext, "-exec", "grep", "--color=always", "-iH", '"{}"'.format(search), "{}", '\\;']
    return(command)

def main():
    command = build_command()
    print(" ".join(command))
    subprocess.Popen(command)

main()
