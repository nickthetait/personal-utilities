import time
import subprocess
import argparse

def get_arg():
    parser = argparse.ArgumentParser("python3 vicious-guard-dog.py")
    parser.add_argument(
        "security_level",
        default="lock",
        nargs='?',
        help="Defines how aggressively to react when time limit is hit. "
           + "'test' to do nothing (useful for debugging). "
           + "'lock' or (empty) to log out user. Default behavior. "
           + "'abort' to shutdown the entire system ASAP!",
        type=str)
    return parser.parse_args().security_level

def main():
    mode = get_arg()
    print("Chihuahua is resting...")

    while(True):
        time.sleep(60*60) #seconds

        if mode == "test":
            input("Test mode active. Aggressive action would normally occur now...")
        elif mode == "abort":
            subprocess.run(["shutdown", "-P", "now"])
        else:
            subprocess.run(["gnome-screensaver-command", "--lock"])

main()
